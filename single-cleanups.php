<?php
/**
 * Template Name: Single Cleanup template
 */

acf_form_head();
get_header();

$post_id = get_the_id();
$location = get_field('address', $post_id);
$user_id = get_current_user_id();
$urlvar = get_query_var( 'state' );


$cleanup_type = get_field('cleanup_type');

//$cleanups = new WP_Query( $args ); ?>
<main class='main-content'>
    <?php	if ($urlvar != 'edit'): ?>
    <div class="acf-container">
        <section class="acf-map">
            <div style="display: none;">
                <?php cleanupPinList($post_id); ?>
            </div>
        </section>
        <div class="loading-animation">
            <div class="uil-poi-css" style="transform:scale(0.6);">
            </div>
        </div>
    </div>
    <?php include(get_stylesheet_directory() . '/template-parts/cleanuplist-middlemenu.php'); ?>
    <?php endif; ?>
    <div class="content grid">

        <div class="cleanup-list">
            <?php
				$location = get_field('address');

			?>
            <?php  if(empty($urlvar)): ?>
            <div class="marker cleanup-group" data-lat="<?php echo $location['lat']; ?>"
                data-lng="<?php echo $location['lng']; ?>">
                <div class="row">
                    <h1><?php echo $post->post_title; ?></h1>
                    <?php if(get_field('description')): ?>
                    <p><?php the_field('description'); ?></p>
                    <?php endif; ?>
                </div>

                <div class="row">
                    <h2>Information</h2>
                    <?php if(get_field('date')): ?>
                    <b><?= translateACF('date'); ?>:
                    </b><?php $start_date = get_field('date'); $start_date = new DateTime($start_date); echo $start_date->format('j M Y'); if (get_field('end_date')) { echo ' - '; $end_date = get_field('end_date'); $end_date = new DateTime($end_date); echo $end_date->format('j M Y'); }; ?>
                    <?php endif; ?>
                    <br>
                    <?php if($cleanup_type): ?>
                    <p><b><?= translateACF('cleanup_type'); ?>: </b>
                        <?php
              $i = 0;
              $len = count($cleanup_type);
               ?>
                        <?php foreach ($cleanup_type as $type): ?>
                        <a href="<?php echo get_term_link($type->term_id); ?>"><?php echo $type->name; ?></a>
                        <?php if($i != $len - 1) { echo ' / '; } ?>
                        <?php $i++; endforeach; ?>
                    </p>
                    <?php endif; ?>

                    <?php if($location['address']): ?>
                    <b><?= translateACF('location'); ?>: </b><a
                        href="<?php echo 'https://www.google.com/maps/place/' . urlencode( $location['address'] ); ?>"
                        class="address"><?php echo $location['address']; ?></a>
                    <?php endif; ?>
                    <?php if(get_field('contact_name')): ?>
                    <p><b>Contact: </b><?php the_field('contact_name'); ?></p>
                    <?php endif; ?>
                    <?php if(get_field('contact_email')): ?>
                    <p><b>Contact email: </b><?php the_field('contact_email'); ?></p>
                    <?php endif; ?>
                    <?php if(!get_field('contact_name') && !get_field('contact_email')): ?>
                    <p><b><?= translateACF('contact'); ?>: </b>No Public contact information provided.</p>
                    <?php endif; ?>
                    <div class="row">
                        <?php
            $number_of_attendees = get_field('number_of_attendees');
            ?>
                        <h2><?php echo $number_of_attendees; ?> <i class="fa fa-users" aria-hidden="true"></i>
                            <?= translateACF('attending'); ?>
                        </h2>
                    </div>
                </div>

            </div>
            <?php endif; ?>

            <div class="row">
                <?php get_template_part('template-parts/edit','link'); ?>
            </div>
        </div>

    </div>
</main>
<?php get_footer(); ?>