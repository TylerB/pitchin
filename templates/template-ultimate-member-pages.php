<?php
/**
 * Template Name: Ultimate Member Page
 */

get_header();

?>

<main class='main-content'>
  <?php include(get_stylesheet_directory() . '/template-parts/flexible-content.php'); ?>
  <?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
        <?php the_content(); ?>
    <?php endwhile; ?>
      <?php // Navigation ?>
    <?php else : ?>
      <?php // No Posts Found ?>
  <?php endif; ?>
</main>

<?php get_footer(); ?>
