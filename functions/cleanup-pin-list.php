<?php 
function cleanupPinList($id) {
  $location = get_field('address', $id);
  ?>
  
  <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
    <div class="content">
      <div class="title col">
        <h2><?php the_title(); ?></h2>
        <p class="address"><?php echo $location['address']; ?></p>
        <a href="<?php the_permalink($id); ?>" class="button cleanup-button">View Cleanup</a>
      </div>
    </div>
  </div>
  <?php
}
?>
