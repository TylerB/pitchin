<?php

// add_action('template_redirect', 'check_if_bags_left');
// function check_if_bags_left(){
//     //I load just before selecting and rendering the template to screen
//     $bags_promised = get_field('total_bags_allocated', 'option');
//     $bags_left = get_field('total_bags_to_distribute', 'option');

//     if ($bags_promised >= ($bags_left - 200) ) {
//       update_field('disable_registration', 1, 'option');
//     }
// }


/**
 * Duplicates a post & its meta and it returns the new duplicated Post ID
 * @param  [int] $post_id The Post you want to clone
 * @return [int] The duplicated Post ID
 */
function duplicate($post_id, $title) {

  // error_log("duplicating $title - $post_id");

  // $title   = get_the_title($post_id . $title);
  $oldpost = get_post($post_id);


  $newTitle = '#' . $post_id . ' hidden ' . $title;
  $post    = array(
    'post_title' => $newTitle,
    'post_date' => $oldpost->post_date,
    'post_status' => 'publish',
    'post_type' => $oldpost->post_type,
    'post_author' => $oldpost->post_author
  );
  $new_post_id = wp_insert_post($post);
  // Copy post metadata
  $data = get_post_custom($post_id);
  foreach ( $data as $key => $values) {
    foreach ($values as $value) {
      add_post_meta( $new_post_id, $key, $value );
    }
  }

  // update_field('hide_on_frontend', 1, $new_post_id);
  wp_set_post_terms( $new_post_id, 'yes', 'hide_cleanup');

  //add this new post to the related cleanups field on the post that's being duplicated
  $related_cleanups_field = get_field('related_cleanups', $post_id);



  if (!is_array($related_cleanups_field)) {
    $related_cleanups = array();
    array_push($related_cleanups, $related_cleanups_field);
  } else {
    $related_cleanups = $related_cleanups_field;
  }

  if (is_array($related_cleanups)) {
    array_push($related_cleanups, $new_post_id);
  }

  $test_value = json_encode($related_cleanups);
  $test_value2 = json_encode($related_cleanups_field);

  // error_log("related_cleanups: $test_value");
  // error_log("related_cleanups_field: $test_value2");
  
  update_field('related_cleanups', $related_cleanups, $post_id);

  return $new_post_id;
}

function my_pre_save_post( $post_id ) {
    if( get_post_type($post_id) != 'cleanups' || get_field('hide_on_frontend_tax', $post_id)) {
        return $post_id;
    }

    $related_cleanups_field_test = get_field('related_cleanups', $post_id);

    // error_log("pre save post $post_id");
    // error_log("related_cleanups_field_test: $related_cleanups_field_test");

    // force uppercase postal codes
    $postal_code = get_field('postal_code', $post_id);
    update_field('postal_code', strtoupper($postal_code), $post_id);

    $cleanup_package_id = get_field('cleanup_package', $post_id);
    $package_weight = get_field('weight_in_grams', $cleanup_package_id);
    $cleanup_bag_amount = get_field('number_of_bags', $cleanup_package_id);
    $cleanup_attendees = get_field('number_of_people', $cleanup_package_id);

    $box_id = 5908; // id of large box cleanup package in cleanup package post type
    $box_weight = get_field('weight_in_grams', $box_id); 
    $box_bag_amount = get_field('number_of_bags', $box_id); 

    $small_box_id = 12718; // id of small box cleanup package
    $small_box_weight = get_field('weight_in_grams', $small_box_id);
    $small_box_bag_amount = get_field('number_of_bags', $small_box_id);
    
    $bag_size = get_field('bag_size', $post_id);
    $bag_type = get_field('bag_type', $post_id);

    $num_cleanups_to_gen = 0;


    // related cleanups are actually just all the extra hidden cleanups generated to add extra lines to the csv export
    $related_cleanups = get_field('related_cleanups', $post_id);
    if ($related_cleanups) {
      foreach ($related_cleanups as $cleanup) {
        wp_delete_post($cleanup, true);
      }
    }
    // error_log('deleting related cleanups...');

    if ($cleanup_package_id != $box_id && $cleanup_package_id != $small_box_id) {
      // error_log('Post is not a custom box order');

      // remove number of participants in case this is a post being edited
      delete_field('number_of_participants_break_to_boxes');

      //? If its not a custom box order
      // then add the cleanup attendees from the cleanup package to the cleanup post
      update_field('number_of_attendees', $cleanup_attendees, $post_id);

      if ($bag_size == 'small' && $cleanup_bag_amount % $small_box_bag_amount == 0) {
        // error_log('bag size small & divisible by boxes');
        //? if small bag size & bag amount is divisible by the small box bag amount
        // get the number of cleanups that should be generated
        $number_of_boxes = $cleanup_bag_amount / $small_box_bag_amount;
        // error_log('small - number_of_boxes');
        // error_log($number_of_boxes);

        $num_cleanups_to_gen = $number_of_boxes - 1;
        
        // error_log('small - num_cleanups_to_gen');
        // error_log($num_cleanups_to_gen);

        // -1 because one post already exists
        
        // set clean_package as the small box package before duplicating the post!!@!
        update_field('cleanup_package', $small_box_id, $post_id);

      } elseif ($bag_size == 'large' && $cleanup_bag_amount % $box_bag_amount == 0) {
          // error_log('bag size large & divisible by boxes');
          //? if large bag size & bag amount is divisible by the large box bag amount
          // get the number of cleanups that should be generated
          $number_of_boxes = $cleanup_bag_amount / $box_bag_amount;
          
          // error_log("large - number_of_boxes: $number_of_boxes");

          $num_cleanups_to_gen = $number_of_boxes - 1;

          // error_log("large - num_cleanups_to_gen: $num_cleanups_to_gen");

          // -1 because one post already exists

          // set clean_package as the large box package before duplicating the post!!@!
          update_field('cleanup_package', $box_id, $post_id);

      }
    } elseif ($cleanup_package_id == $box_id || $cleanup_package_id == $small_box_id) {
      // error_log('Post is a custom box order!');
      //? If its a custom box order
      //? we already have number of participants
      $total_participants = get_field(
        'number_of_participants_break_to_boxes', $post_id
      );

      // error_log("total participants: $total_participants");

      //* figure out how many boxes for the number of participants
      $unrounded_total = $total_participants / 1000;
      $number_of_thousands = round($unrounded_total);
      
      if ($bag_size == 'small') {
        $boxes_per_thousand = 3;
      } else {
        $boxes_per_thousand = 4;
      }

      $number_of_boxes = $number_of_thousands * $boxes_per_thousand;
      $num_cleanups_to_gen = $number_of_boxes - 1;
    }

      $num_attend = get_field(
        'number_of_participants_break_to_boxes', $post_id
      );

      if ($num_attend) {
        update_field('number_of_attendees', $num_attend, $post_id);
      }

    // only allow max 20 boxes to be sent out
    // one cleanup already exists so only generate 19 more.
    if ($num_cleanups_to_gen > 19) {
      $num_cleanups_to_gen = 19;
    };

    // error_log("number of extra cleanup posts to generate: $num_cleanups_to_gen");
    // error_log("bag size: $bag_size");
    // error_log("bag type: $bag_type");
    if ($num_cleanups_to_gen >= 1 && $bag_type !== 'no_bags') {
        //generate the extra hidden cleanups
        for ($i = 1; $i <= $num_cleanups_to_gen; $i++) {
          $sendTitle = 
            '(box '. ($i + 1) .' of '. ($num_cleanups_to_gen + 1) .')';

          duplicate($post_id, $sendTitle);
        }
      }
  
    return $post_id;

}

add_filter('acf/save_post' , 'my_pre_save_post', 10, 1 );

function custom_query_vars_filter($vars) {
  $vars[] = 'created';
  $vars[] = 'state';
  $vars[] = 'yr';
  return $vars;
}

add_filter( 'query_vars', 'custom_query_vars_filter' );

?>