<?php
function ps_acf_save_post( $post_id ) {
    // Don't do this on the ACF post type
    // if ( get_post_type( $post_id ) == 'acf' ) return;
    // Only do this on the cleanups post type
    if ( get_post_type( $post_id ) != 'cleanups' ) return;

    // Get the Fields
    $fields = get_field_objects( $post_id );

    // Prevent Infinite Looping...
    remove_action( 'acf/save_post', 'my_acf_save_post' );

    $newTitle = $fields['organization_name']['value'] . '  #' . $post_id;

    // Grab Post Data from the Form
    $post = array(
        'ID'           => $post_id,
        'post_type'    => 'cleanups',
        'post_title'   => $newTitle,
        'post_status'  => 'publish'
    );

    // Update the Post
    wp_update_post( $post );

    $updated_post = get_post($post_id);
    $author_id = $updated_post->post_author;
   
    $author_email = get_the_author_meta( 'user_email', $author_id );

    // get the postal code and format the value
    $postal_code = get_field('postal_code', $post_id);
    $formatted_postal_code = strtoupper(str_replace( ' ', '',  $postal_code ));
    update_field('postal_code', $formatted_postal_code, $post_id);
    
    update_field('email', $author_email, $post_id);

    // Continue save action
    add_action( 'acf/save_post', 'my_save_post' );

    // Set the Return URL in Case of 'new' Post
    $_POST['return'] = add_query_arg( 'updated', 'true', get_permalink( $post_id ) );
}
add_action( 'acf/save_post', 'ps_acf_save_post', 10, 1 );

 ?>
