<?php 
	
	/*
		*** IMPORTANT NOTE/WARNING ***
		If your options page includes very large field groups that use repeaters
		flex fields or clone fields that there is a lot of data in these fields
		using and object buffer on the page can very likely cause the use of too
		much memory and crash your options page.
		
		
		At the time that I am setting up this example it is currently not possible to
		modify an ACF Options Page beyond the settings in the options page and adding
		field groups.
		
		This file gives an example of how to insert content into several places in an
		options page.
		
		A note about the action hooks to use: You must know the action hook that will be used by 
		WP to call the callback function when ACF uses add_menu_page() and add_submenu_page(). 
		The hook is not returned so you will need to figure out what it is.
		
		top level options page hook = "toplevel_page_{$acf_menu_slug}"
		
		if top level page redirects to first sub level page use sub options page slug
		
		sub options page hook = "{$acf_parent_slug}_page_{$acf_sub_page_slug}"
		
		If your having trouble with the hook, to figure out what the hook really is
		open the file
		/advanced-custom-fields-pro/pro/admin/options-page.php
		find the function html, currently on line 459
		add this code to the top of the function
		echo '<br><br>',current_filter(),'<br><br>';
		This will output the correct hook to use for your actions for the options page
		don't forget to remove the test code
		
	*/
	
	/*
		create an action for your options page that will run before the ACF callback function
		see above for information on the hook you need to use
	*/
	add_action('toplevel_page_piw-settings', 'before_acf_options_page', 1);
	function before_acf_options_page() {
		/*
			Before ACF outputs the options page content
			start an object buffer so that we can capture the output
		*/
		ob_start();
	}
	
	/*
		create an action for your options page that will run after the ACF callback function
		see above for information on the hook you need to use
	*/
	add_action('toplevel_page_piw-settings', 'after_acf_options_page', 20);
	function after_acf_options_page() {
		/*
			After ACF finishes get the output and modify it
		*/
		$content = ob_get_clean();
		
		$count = 1; // the number of times we should replace any string

		$year = date('Y');
		$month = date('F');
		
        $total_bags_allocated_query = new WP_Query([
            'post_type' => 'cleanups',
            'posts_per_page' => -1,
            'date_query' => array(
                array(
                    'year'  => $year
                ),
            ),
		]);

        $total_this_month_query = new WP_Query([
            'post_type' => 'cleanups',
            'posts_per_page' => -1,
            'date_query' => array(
                array(
					'year'  => $year,
					'month' => date('n')
                ),
            ),
		]);

		
		
		// insert something before the <h1>
        $my_content = "<h2>Total cleanups in $year = $total_bags_allocated_query->post_count</h2>";

		$query_bag_count = 0;
		$query_small_bag_count = 0;

        if ($total_bags_allocated_query->have_posts()) {
            while($total_bags_allocated_query->have_posts()) {
				$total_bags_allocated_query->the_post();

				$bag_size = get_field('bag_size');
                $bag_type = get_field('bag_type');

                if ($bag_type !== 'no_bags') {
                    $cleanup_package = get_field('cleanup_package');

                    $number_of_bags = get_field(
                        'number_of_bags', $cleanup_package
					);

					if ($bag_size !== 'small') {
						$query_bag_count = $query_bag_count + $number_of_bags;
					} elseif ($bag_size === 'small') {
						$query_small_bag_count = $query_small_bag_count + $number_of_bags;
					}
					
                } 
            }
        }
		
		$my_content .= "<h2>Large bags allocated in $year = $query_bag_count</h2>
			<h2>Small bags allocated in $year = $query_small_bag_count</h2>
			<p>Total cleanups in $month $year = $total_this_month_query->post_count</p>
		";


		$month_query_bag_count = 0;
		$month_query_small_bag_count = 0;

		if ($total_this_month_query->have_posts()) {
            while($total_this_month_query->have_posts()) {
				$total_this_month_query->the_post();

				$bag_size = get_field('bag_size');
                $bag_type = get_field('bag_type');

                if ($bag_type !== 'no_bags') {
                    $cleanup_package = get_field('cleanup_package');

                    $number_of_bags = get_field(
                        'number_of_bags', $cleanup_package
					);

					if ($bag_size !== 'small') {
						$month_query_bag_count = $month_query_bag_count + $number_of_bags;
					} elseif ($bag_size === 'small') {
						$month_query_small_bag_count = $month_query_small_bag_count + $number_of_bags;
					}
					
                } 
            }
        }


		$content .= "
			<p>Large bags allocated in $month $year = $month_query_bag_count</p>
			<p>Small bags allocated in $month $year = $month_query_small_bag_count</p>
		";

		
		$content = str_replace('</form>', '</form>'.$my_content, $content, $count);
	

		// output the new content
		echo $content;
	}
	
?>