<?php

add_filter ( 'woocommerce_account_menu_items', 'add_cleanups_link' );
function add_cleanups_link( $menu_links ){
 
	// we will hook "cleanups_link" later
	$new = array( 'cleanups_link' => 'My Cleanups' );
 
	// or in case you need 2 links
	// $new = array( 'link1' => 'Link 1', 'link2' => 'Link 2' );
 
	// array_slice() is good when you want to add an element between the other ones
	$menu_links = array_slice( $menu_links, 0, 1, true ) 
	+ $new 
	+ array_slice( $menu_links, 1, NULL, true );
 
 
	return $menu_links;
 
 
}
 
add_filter( 'woocommerce_get_endpoint_url', 'cleanups_link_endpoint', 10, 4 );
function cleanups_link_endpoint( $url, $endpoint, $value, $permalink ){
 
	if( $endpoint === 'cleanups_link' ) {

		$url = '/pitch-in-week/my-cleanups/';
 
	}
	return $url;
 
}