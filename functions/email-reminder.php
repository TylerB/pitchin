<?php

function wpse27856_set_content_type(){
    return "text/html";
}
add_filter( 'wp_mail_content_type','wpse27856_set_content_type' );


//remind user of cleanup 1 week prior
function emailReminder() {
  //$date = date('Ymd', strtotime('+0 days'));
  $date = date('Ymd', strtotime('+7 days'));

  $args = array(
  	'numberposts'	=> -1,
  	'post_type'		=> 'cleanups',
  	'meta_key'		=> 'date',
  	'meta_value'	=> $date
  );

$upcoming_query = new WP_Query($args);

// $volunteers = array();

if ($upcoming_query->have_posts()) {
  while ($upcoming_query->have_posts()) {
    $upcoming_query->the_post();
    $post_volunteers = get_field('volunteers', get_the_id());
    $page_link = get_page_link();
    $the_title = get_the_title();
    $the_address = get_field('address', get_the_id());
    $the_time = get_field('time', get_the_id());
    $the_date = get_field('date', get_the_id());
    foreach ($post_volunteers as $volunteer) {

      $to = $volunteer['user_email'];
      $subject = 'Your Cleanup is in one week!';
      $message = 'Hi '.$volunteer['user_firstname'].', <br>Your PITCH-IN Week cleanup <a href="'. $page_link .'">"'. $the_title .'"</a> is one week from now!
      Please meet the group at '. $the_time .' on '. date('l', strtotime($the_date)).' the '.date('jS', strtotime($the_date)). '.<br> <a href="'.$page_link.'">Go to the cleanup page to see the location.</a> <br><br>Best Wishes,<br>PITCH-IN Canada';

      wp_mail( $to, $subject, $message );
      // echo $message;
    }
  }
}
// print_r($volunteers);
wp_reset_postdata();
// die();
}
add_action( 'wp_ajax_emailReminder', 'emailReminder' );
add_action( 'wp_ajax_nopriv_emailReminder', 'emailReminder' );


// add_action( 'auto_emailer',  'emailReminder' );
//
// wp_schedule_event( time(), 'hourly', 'auto_emailer' );



//
// register_activation_hook(__FILE__, 'my_activation');
//
// function my_activation() {
//     if (! wp_next_scheduled ( 'my_hourly_event' )) {
// 	wp_schedule_event(time(), 'hourly', 'my_hourly_event');
//     }
// }
//
// add_action('my_hourly_event', 'emailReminder');
//
// register_deactivation_hook(__FILE__, 'my_deactivation');
//
// function my_deactivation() {
// 	wp_clear_scheduled_hook('my_hourly_event');
// }

// //change title label in frontend form
// function my_acf_prepare_field( $field ) {
//
//     // if ( is_page_template('page-register-cleanup.php') ) {
//     //    $field['label'] = "Your cleanup title";
//     //   //  $field['instructions'] = "Changed Instruction";
//     // }
//
//     if ( is_page_template('page-register-group.php') ) {
//        $field['label'] = "Your group/organization name";
//        //$field['instructions'] = "Changed Instruction";
//     }
//
//     if ( $field ) {
//         return $field;
//     } else {
//         exit;
//     }
// }
// add_filter('acf/prepare_field/name=_post_title', 'my_acf_prepare_field');
//
// add_filter( 'acf/get_valid_field', 'change_input_labels');
// function change_input_labels($field) {
//
//   $title_label = '';
//
//   if ( is_page_template('page-register-cleanup.php') ) {
//     $title_label = 'Custom title cleanup';
//   }
//
//   if ( is_page_template( 'page-register-group.php' )) {
//     $title_label = 'Custom title group';
//   }
//
//   if($field['name'] == '_post_title') {
//     $field['label'] = $title_label;
// 	}
//
// 	return $field;
//
// }

 ?>
