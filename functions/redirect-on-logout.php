<?php

// add_action('wp_logout','pi_redirect_after_logout');
// function pi_redirect_after_logout( ){

//     wp_redirect( get_home_url() );
    
//     exit();
// }


add_filter( 'logout_url', 'pro_logout_url' );
 
function pro_logout_url( $url ) {
   
   $args = array( 'action' => 'logout', 'redirect_to' => apply_filters( 'wpml_home_url', get_option( 'home' ) ) );
 
   $logout_url = add_query_arg( $args, site_url( 'wp-login.php', 'login' ) );
   $logout_url = wp_nonce_url( $logout_url, 'log-out' );
 
   return $logout_url;
 
}