<?php

add_action('wp_enqueue_scripts', 'aa_scripts');

function aa_scripts() {
  wp_register_style('aa_style', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
  wp_enqueue_style('aa_style'); // Enqueue it!

  wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', array(), '4.7.0' );
  wp_enqueue_script('jquery'); // Enqueue it!

  wp_register_script('googleMapApi', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyB7C00SeJl7wEaoMwrehvMVJGfW0YJD440&libraries=places'); // Custom scripts
  wp_enqueue_script('googleMapApi'); // Enqueue it!

  wp_register_script('sentry', 'https://browser.sentry-cdn.com/5.10.2/bundle.min.js');
  wp_enqueue_script('sentry');

  wp_enqueue_script('map', get_stylesheet_directory_uri() . '/assets/js/map.js', array('jquery', 'googleMapApi'), null, false);

  // wp_register_script('vendorJs', get_template_directory_uri() . '/assets/js/vendors.js'); // Custom scripts
  // wp_enqueue_script('vendorJs', array( 'jquery' )); // Enqueue it!

  wp_register_script('aa_customJs', get_template_directory_uri() . '/assets/js/custom.js'); // Custom scripts
  wp_enqueue_script('aa_customJs', array( 'jquery', 'map-functions' )); // Enqueue it!

  // global $wp_query;
  // wp_localize_script( 'aa_customJs', 'ajaxpagination', array(
  // 	'ajaxurl' => admin_url( 'admin-ajax.php' ),
  // 	'query_vars' => json_encode( $wp_query->query )
  // ));

  // $translation_array = array( 'templateUrl' => get_stylesheet_directory_uri() );
  //after wp_enqueue_script
  // wp_localize_script( 'aa_customJs', 'object_name', $translation_array );

}

?>