<?php

add_action('um_after_new_user_register','update_user_meta_woo');

function update_user_meta_woo($user_id){
  $first_name = get_user_meta($user_id, 'first_name', true);
  $last_name = get_user_meta($user_id, 'last_name', true);
  update_user_meta($user_id, 'billing_first_name', $first_name);
  update_user_meta($user_id, 'shipping_first_name', $first_name);
  update_user_meta($user_id, 'billing_first_name', $last_name);
  update_user_meta($user_id, 'shipping_last_name', $last_name);

  $user_info = get_userdata($user_id);
  $email = $user_info->user_email;
  update_user_meta($user_id, 'billing_email', $last_name);

  $address1 = get_user_meta($user_id, 'billing_address_1', true);
  $address2 = get_user_meta($user_id, 'billing_address_2', true);
  update_user_meta($user_id, 'shipping_address_1', $address1);
  update_user_meta($user_id, 'shipping_address_2', $address2);

  $city = get_user_meta($user_id, 'shipping_city', true);
  update_user_meta($user_id, 'shipping_address_1', $city);

  $postcode = get_user_meta($user_id, 'billing_postcode', true);
  update_user_meta($user_id, 'shipping_postcode', $postcode);

  $country = get_user_meta($user_id, 'billing_country', true);
  update_user_meta($user_id, 'shipping_country', $country);

  $state = get_user_meta($user_id, 'billing_state', true);
  update_user_meta($user_id, 'shipping_state', $state);

}


function hide_menu_message() {
  $user_id = get_current_user_id();

  $cleanups = get_posts(array(
    'post_type' => 'cleanups',
    'author' => $user_id
  ));

  foreach($cleanups as $cleanup) {
    update_field('hide_order_menu_message', 1, $cleanup->ID);
  }
}
add_action( 'wp_ajax_hide_menu_message', 'hide_menu_message' );
add_action( 'wp_ajax_nopriv_hide_menu_message', 'hide_menu_message' );


/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 1000; /* pixels */
}

/**
 * Neat functions and definitions
 *
 * @package Neat
 */

/**
 * Paths
 *
 * @since  1.0
 */
if ( !defined( 'AA_THEME_DIR' ) ){
    define('AA_THEME_DIR', ABSPATH . 'wp-content/themes/' . get_template());
}

function rudr_filter_by_the_author() {
	$params = array(
		'name' => 'author', // this is the "name" attribute for filter <select>
		'show_option_all' => 'All authors' // label for all authors (display posts without filter)
	);

	if ( isset($_GET['user']) )
		$params['selected'] = $_GET['user']; // choose selected user by $_GET variable

	wp_dropdown_users( $params ); // print the ready author list
}

add_action('restrict_manage_posts', 'rudr_filter_by_the_author');

// function get_user_id() {
//   return get_current_user_id();
// }
//
// add_shortcode( 'user_id', 'get_user_id' );

 ?>
