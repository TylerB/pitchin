<?php

//add or remove volunteer from cleanup via ajax
function add_user_to_cleanup() {
  $add_remove = $_POST['add_remove'];
  $cleanup_id = $_POST['cleanup_id'];
  $user_id = get_current_user_id();

  $volunteers = get_field('field_57e89bd93b402', $cleanup_id, false);
  $cleanups = get_field('user_cleanups', 'user_'.$user_id, false);

  if (empty($volunteers)) {
    $volunteers = array();
  }

  if (empty($cleanups)) {
    $cleanups = array();
  }

  if ($add_remove == 'add' && !in_array($user_id, $volunteers)) {
    //if user is not in volunteers array then add them
      array_push($volunteers, $user_id);
      array_push($cleanups, $cleanup_id);

      $response = "<p>You're registered to attend this cleanup!<br>You'll receive a reminder email one week before the event.</p>";
  } elseif ($add_remove == 'remove') {
    //find user in array and remove them
    $pos = array_search($user_id, $volunteers);
    unset($volunteers[$pos]);

    $pos = array_search($cleanup_id, $cleanups);
    unset($cleanups[$pos]);

    $response = "You've been successfully removed from this cleanup.<br>You'll no longer receieve an email reminder before the event.";
  }

  update_field('field_57e89bd93b402', $volunteers, $cleanup_id);
  update_field('user_cleanups', $cleanups, 'user_'.$user_id);

  echo $response;
  die();
}
add_action( 'wp_ajax_add_user_to_cleanup', 'add_user_to_cleanup' );
add_action( 'wp_ajax_nopriv_add_user_to_cleanup', 'add_user_to_cleanup' );


//get middle menu template part
function middle_menu_refresh() {
  include_once('template-parts/cleanuplist-middlemenu.php');
  // return $html;
  die();
}
add_action( 'wp_ajax_middle_menu_refresh', 'middle_menu_refresh' );
add_action( 'wp_ajax_nopriv_middle_menu_refresh', 'middle_menu_refresh' );

 ?>
