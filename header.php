<?php
/**
 * The header for our theme.
 *
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:title" content="PITCH-IN - <?php echo the_title(); ?>" />
    <meta property="og:url" content="<?php echo the_permalink(); ?>" />
    <meta property="og:site_name" content="PITCH-IN Canada" />
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <script type="text/javascript">
    var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
    var current_post_id = "<?php echo get_the_ID(); ?>";
    </script>
    <?php wp_head(); ?>

    <?php if (get_field('disable_registration', 'option')) {
  ?><style>
    .registration-closed-hide {
        display: none !important;
    }
    </style><?php
} ?>
</head>

<?php
if ( is_user_logged_in() ):
    $current_user = wp_get_current_user();
endif;
 ?>

<body <?php body_class(); ?>>
    <section class="all-but-footer">
        <nav class='menu-main-wrapper'>
            <div class="topbar">
                <div class="grid">
                    <div class="social-icons">
                        <?php if ( have_rows('social_icons', 'option') ): ?>
                        <?php while ( have_rows('social_icons', 'option') ) : the_row(); ?>
                        <a target='_blank' href="<?php the_sub_field('link'); ?>" class='social-icon'>
                            <i class="fa <?php the_sub_field('icon'); ?>"></i>
                        </a>
                        <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                    <div class="lang">
                        <?php custom_language_switcher(); ?>
                    </div>
                </div>
            </div>
            <div class="inner grid">

                <a href="<?php echo get_home_url(); ?>" class='logo'>
                    <img src="<?php the_field('logo', 'option'); ?>" alt="logo">
                </a>

                <div class="menu-wrap-hide">
                    <nav class='menu-main'>
                        <?php wp_nav_menu(array(
              'theme_location' => 'primary'
            )); ?>
                    </nav>
                    <nav class="menu-icons">
                        <ul>
                            <?php 
                                $cartcount = WC()->cart->get_cart_contents_count(); 
                                if ($cartcount > 99):
                                    $cartcount = '99+';
                                endif;
                            ?>
                            <li class='menu-item cart-icon'>
                                <a href="<?= get_permalink(12512); ?>"><i class="fa fa-shopping-cart"
                                        aria-hidden="true">
                                        <div
                                            class="cart-icon__badge <?php if ($cartcount === 0): echo 'badge-hidden'; endif; ?>">
                                            <div class="cart-count"><?php echo $cartcount ?></div>
                                        </div>
                                    </i></a>
                            </li>
                            <li class='menu-item'>
                                <a href="<?php echo get_permalink(12496); ?>"><i class="fa fa-user"
                                        aria-hidden="true"></i></a>

                                <?php if(is_user_logged_in()): ?>
                                <ul class="sub-menu">

                                    <li><?php apply_filters( 'wpml_element_link', 12496, 'page' ) // my account ?></li>
                                    <li><?php apply_filters( 'wpml_element_link', 959, 'page' ) // my cleanups ?></li>
                                    <li><?php apply_filters( 'wpml_element_link', 12494, 'page' ) // shopping cart ?>
                                    </li>
                                    <li><a href="<?php echo wp_logout_url(); ?>">Log out</a></li>
                                </ul>
                                <?php endif; ?>
                            </li>
                        </ul>
                    </nav>
                </div>

                <div class="mobile-menu-button">
                    <span class="lines"></span>
                </div>
            </div>
        </nav>

        <?php $media_type = get_field('media_type'); ?>
        <?php if(get_field('show_header') && get_post_type() != 'cleanups' && get_post_type() != 'groups'): ?>
        <header class='
                header-main
                <?php if(get_field('shrink_header') == true): ?>
                  shrink-header
                <?php endif; ?>
                <?php if(get_field('animate_zoom_header') == true): ?>
                  wpb_appear wpb_start_animation
                <?php endif; ?>
                <?php if(get_field('center_text') == true): ?>
                  center-text
                <?php endif; ?>
                ' <?php
  if($media_type == 'Video'): ?> data-vide-bg="mp4: <?php the_field('mp4'); ?>"
            data-vide-options="posterType: jpg, loop: true, muted: true, position: 0% 0%, bgColor: 'rgba(12, 162, 108, .77)'"
            <?php elseif($media_type == 'Image' && get_field('image')): ?>
            style="background-image: url('<?php the_field('image'); ?>')" <?php endif; ?>>
            <div class="
    <?php if(get_field('bg_overlay') == true): ?>
      bg-overlay
    <?php endif; ?>
  ">

                <div class="inner grid">

                    <div class="text">
                        <?php if(!is_user_logged_in() || get_field('copy_from_logged_out')): ?>
                        <?php if(get_field('above_heading')): ?><h2><?php the_field('above_heading'); ?></h2>
                        <?php endif; ?>
                        <?php if(get_field('heading')): ?><h1><?php the_field('heading'); ?></h1><?php endif; ?>
                        <?php if(get_field('below_heading')): ?><h2><?php the_field('below_heading'); ?></h2>
                        <?php endif; ?>
                        <?php else: ?>
                        <?php if(get_field('above_heading_loggedin')): ?><h2>
                            <?php the_field('above_heading_loggedin'); ?></h2><?php endif; ?>
                        <?php if(get_field('heading_loggedin')): ?><h1><?php the_field('heading_loggedin'); ?></h1>
                        <?php endif; ?>
                        <?php if(get_field('below_heading_loggedin')): ?><h2>
                            <?php the_field('below_heading_loggedin'); ?></h2><?php endif; ?>
                        <?php endif; ?>
                    </div>

                    <?php if(!is_user_logged_in() || get_field('copy_from_logged_out')): ?>
                    <?php if(have_rows('buttons')): ?>
                    <div class="button-wrapper">
                        <?php while(have_rows('buttons')): the_row(); ?>
                        <a href="<?php the_sub_field('button_link_internal'); the_sub_field('button_link_external'); ?>"
                            class="button">
                            <?php the_sub_field('button_text'); ?>
                        </a>
                        <?php endwhile;?>
                    </div>
                    <?php endif; ?>
                    <?php else: ?>
                    <?php if(have_rows('buttons_loggedin')): ?>
                    <div class="button-wrapper">
                        <?php while(have_rows('buttons_loggedin')): the_row(); ?>
                        <a href="<?php the_sub_field('button_link_internal'); the_sub_field('button_link_external'); ?>"
                            class="button">
                            <?php the_sub_field('button_text'); ?>
                        </a>
                        <?php endwhile;?>
                    </div>
                    <?php endif; ?>

                    <?php endif; ?>


                </div>

            </div>
        </header>
        <?php endif; ?>