<?php
/**
 * Front page
 */

get_header();

?>
  <main class='main-content'>
    <div class="content">

    <section class="bg-light-grey grid">
      <?php if ( have_rows('link_boxes') ): ?>
        <section class="icon-boxes grid">
          <?php $isRegistrationDisabled = get_field('disable_registration', 'option'); ?>
          <?php while ( have_rows('link_boxes') ) : the_row(); ?>

            <?php if($isRegistrationDisabled): //display the registration closed content ?>

              <?php $copyFromSignedOut = get_sub_field('copy_content_from_signed_out'); ?>

              <?php if ($copyFromSignedOut): ?>
                <?php $link = get_sub_field('link_signed_out'); ?>
                <?php if($link): ?>
                <a class="icon-box" href='<?php echo $link['url']; ?>' target='<?php echo $link['target']; ?>'>
                  <div class='icon-box__icon'>
                    <?php the_sub_field('icon_signed_out'); ?>
                  </div>

                  <h2 class="icon-box__title">
                    <?php echo $link['title']; ?>
                  </h2>
                  <div class='icon-box__paragraph'>
                    <?php the_sub_field('paragraph_signed_out'); ?>
                  </div>
                </a>
                <?php endif; ?>
              <?php else: ?>
                <?php $link = get_sub_field('link_registration_disabled'); ?>
                <?php if($link): ?>
                  <a class="icon-box" href='<?php echo $link['url']; ?>' target='<?php echo $link['target']; ?>'>
                    <div class='icon-box__icon'>
                      <?php the_sub_field('icon_registration_disabled'); ?>
                    </div>

                    <h2 class="icon-box__title">
                      <?php echo $link['title']; ?>
                    </h2>
                    <?php $paragraph = get_sub_field('paragraph_registration_disabled'); ?>
                    <?php if ($paragraph): ?>
                      <div class='icon-box__paragraph'>
                        <?php echo $paragraph; ?>
                      </div>
                    <?php endif; ?>
                  </a>
                <?php endif; ?>
              <?php endif; ?>

            <?php else: // registration not disabled ?>

              <?php if(is_user_logged_in()): //user is logged in ?>
                <?php $link = get_sub_field('link'); ?>
                <?php if($link): ?>
                <a class="icon-box" href='<?php echo $link['url']; ?>' target='<?php echo $link['target']; ?>'>
                  <div class='icon-box__icon'>
                    <?php the_sub_field('icon'); ?>
                  </div>

                  <h2 class="icon-box__title">
                    <?php echo $link['title']; ?>
                  </h2>
                  <?php $paragraph = get_sub_field('paragraph'); ?>
                  <?php if ($paragraph): ?>
                    <div class='icon-box__paragraph'>
                      <?php echo $paragraph; ?>
                    </div>
                  <?php endif; ?>
                </a>
                <?php endif; ?>

              <?php else: //user is not logged in and registration is not disabled ?>
                <?php $link = get_sub_field('link_signed_out'); ?>
                <?php if($link): ?>
                <a class="icon-box" href='<?php echo $link['url']; ?>' target='<?php echo $link['target']; ?>'>
                  <div class='icon-box__icon'>
                    <?php the_sub_field('icon_signed_out'); ?>
                  </div>

                  <h2 class="icon-box__title">
                    <?php echo $link['title']; ?>
                  </h2>
                  <div class='icon-box__paragraph'>
                    <?php the_sub_field('paragraph_signed_out'); ?>
                  </div>
                </a>
                <?php endif; ?>
              <?php endif; // end is user logged in ?>

            <?php endif;//end is registration disabled ?>

          <?php endwhile; ?>
        </section>
    </section>
  <?php endif; ?>

      <section class="two-column section grid">
        <div>
          <a class="twitter-timeline" data-height="400" data-width="500" data-link-color="#0ca26c" href="https://twitter.com/Pitch_In_Canada">Tweets by Pitch_In_Canada</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
        </div>
        <div>
          <?php if (get_field('lower_text_area_title')): ?>
            <h2><?php the_field('lower_text_area_title'); ?></h2>
          <?php endif; ?>

          <?php if(get_field('lower_text_area_paragraph')): ?>
                <?php the_field('lower_text_area_paragraph'); ?>
          <?php endif; ?>
          <p><a href="//localhost:3000/blog/"><img class="alignnone size-thumbnail wp-image-5368" src="//localhost:3000/wp-content/uploads/2015/12/secretlondon-Green-present-300px-150x150.png" alt="" srcset="//localhost:3000/wp-content/uploads/2015/12/secretlondon-Green-present-300px-150x150.png 150w, //localhost:3000/wp-content/uploads/2015/12/secretlondon-Green-present-300px-180x180.png 180w, //localhost:3000/wp-content/uploads/2015/12/secretlondon-Green-present-300px-300x300.png 300w" sizes="(max-width: 150px) 100vw, 150px" width="150" height="150"></a></p>
        </div>
      </section>
      </div>

    </div>
    <!-- /.content -->
  </main>

  <?php get_footer(); ?>
