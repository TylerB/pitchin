<?php
/**
 * Index File
 */

get_header();

?>
<?php if(has_post_thumbnail()): ?>
  <?php $thumb_id = get_post_thumbnail_id(); ?>
  <?php $thumb_url = wp_get_attachment_image_src($thumb_id,'large', true); ?>
<?php endif; ?>

<header class='header-main header-blog'
style="
background-image: url('<?php echo $thumb_url[0]; ?>');
background-size: cover;
background-position: center top;">
  <div class="bg-overlay">

    <div class="inner grid">
      <div class="text">
        <h1><?php the_title(); ?></h1>
        <p>Posted: <i><b><?php the_time('m/j/y') ?></b></i></p>
      </div>
    </div>

  </div>
</header>
<main class='main-content'>

  <?php include(get_stylesheet_directory() . '/template-parts/flexible-content.php'); ?>

	<div class="content grid single">

<?php if(have_posts()): ?>

  <?php while(have_posts()): the_post(); ?>
    <?php the_content(); ?>

    <div class="single__meta-footer">
      <p><i>This article was published on: <b><?php the_time('m/j/y g:i A') ?></b></i></p>
    </div>
  <?php endwhile; ?>

<?php endif; ?>
	</div>
	<!-- /.content -->
</main>

<?php get_footer(); ?>
