<?php
$functionsDir = get_stylesheet_directory() . '/functions/';
$files = scandir($functionsDir);
$files = glob($functionsDir . '*.php', GLOB_BRACE);

foreach($files as $file) {
  include($file);
}
