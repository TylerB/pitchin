<section class="cleanup-list">

    <h2 class='cleanup-list-title'>Cleanups organized by this group</h2>

    <?php foreach($cleanups as $cleanup) {
    //https://www.google.com/maps/place/' . urlencode( $location['address'] ) .;
    $cleanup_id = $cleanup->ID;
    $location = get_field('address', $cleanup_id); ?>

    <div class="marker card" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
        <?php echo '<p><a href="'. $cleanup->guid  .'"><img src="http://maps.googleapis.com/maps/api/staticmap?center=' . urlencode( $location['lat'] . ',' . $location['lng'] ). '&zoom=13&size=200x204&maptype=roadmap&sensor=false&markers=color:red%7C' . $location['lat'] . ',' . $location['lng'] . '&key=AIzaSyB7C00SeJl7wEaoMwrehvMVJGfW0YJD440" /></a></p>'; ?>
        <div class="content">
            <div class="title col">
                <a href="<?php echo $cleanup->guid; ?>">
                    <h2><?php echo $cleanup->post_title; ?></h2>
                </a>
                <p class="address"><?php echo $location['address']; ?></p>
            </div>

            <div class="group col">
                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            </div>

            <div class="date-container col">
                <div class="date">
                    <?php the_field('date'); ?>
                </div>
            </div>
        </div>
        <section
            class="counter <?php if (in_array($user_id, $volunteers) || $author_check): echo 'attending-this'; endif; ?>">
            <div class="attendee-count">
                <div class="number"><?php the_field('number_of_attendees', $cleanup_id); ?> <i class="fa fa-users"
                        aria-hidden="true"></i></div>
                <div class="subtitle">Attending <?php if($author_check): echo 'your'; else: echo 'this'; endif;?>
                    cleanup</div>
            </div>
            <ul class="social-share">
                <li><a target='_blank'
                        href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink($cleanup_id); ?>"><i
                            class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
                <li><a target='_blank' href="https://twitter.com/home?status=<?php the_permalink($cleanup_id); ?>"><i
                            class="fa fa-twitter" aria-hidden="true"></i></a></li>
            </ul>

        </section>
    </div>

    <?php } ?>

</section>