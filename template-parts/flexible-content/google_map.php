<?php if( have_rows('google_map') ): ?>
	<div class="acf-container acf-map acf-map--short">
		<?php while ( have_rows('google_map') ) : the_row();

			$location = get_sub_field('map_pin');

			?>
			<section class="acf-map__hidden">
				<div
	      class="marker"
	      data-lat="<?php echo $location['lat']; ?>"
	      data-lng="<?php echo $location['lng']; ?>">
					<h4><?php get_sub_field('title'); ?></h4>
					<?php $title = get_sub_field('title'); ?>
					<?php if($title): ?>
							<h3 class='h4'><?php echo $title; ?></h3>
					<?php endif; ?>
					<?php $description = get_sub_field('description'); ?>
					<?php if($description): ?>
							<p><?php echo $description; ?></p>
					<?php endif; ?>
					<?php

					$link = get_sub_field('link');

					if( $link ): ?>

						<a class="button button-style-1" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>

					<?php endif; ?>
				</div>
			</section>
	<?php endwhile; ?>
	</div>
<?php endif; ?>
