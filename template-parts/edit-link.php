<?php
$user_id = get_current_user_id();
$urlvar = get_query_var( 'state' );
$post_type = get_post_type();
$post = get_queried_object();
$post_type_object = get_post_type_object(get_post_type($post));

if (($post->post_author == $user_id) && $urlvar == 'edit'):
  get_template_part('template-parts/edit', 'post'); ?>

<a class='button attend-button' href="<?php the_permalink(); ?>" class="button">Cancel</a>
<!-- <div class='button attend-button delete-post-button'>Delete</div> -->
<div class="delete-modal-bg" style='display: none;'>
  <div class="delete-modal">
    <h2>Deleting <?php the_title(); ?></h2>
    <p>
      <b>This is not reversible.</b>
      <br>Are you sure you want to do this?
      <br><b>There's no going back and you won't receive your supplies in the mail.</b>
    </p>
    <div class="button attend-button nevermind">Nevermind</div>
    <a href="<?php echo get_delete_post_link( get_the_ID() ); ?>" class="button attend-button">Yes, I'm sure</a>
  </div>
</div>
<?php
elseif ($post->post_author == $user_id): ?>
<div class="row">
  <a class='button attend-button' href="<?php the_permalink(); echo '?state=edit' ?>" class="button">Edit <?php echo esc_html($post_type_object->labels->singular_name); ?></a>
  <?php if($post_type == 'groups'): ?>
    <a href="<?php the_permalink(885); ?>" class="button attend-button">Register cleanup</a>
  <?php endif; ?>
</div>
<?php endif; ?>
