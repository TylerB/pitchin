<?php
$user_id = get_current_user_id();
$volunteers = get_field('volunteers', $post_id, false);
if (gettype($volunteers) == 'string') {
  $volunteers = array($volunteers);
}

if (empty($volunteers)) {
  $volunteers = array();
}
?>

<?php if ($post->post_author != $user_id && !is_user_logged_in()): ?>
  <a href="<?php echo wp_registration_url(); ?>" class="button attend-button">Register to attend</a> or <a href="<?php echo wp_login_url( get_permalink() ); ?>" class="button attend-button">Log in</a>
<?php elseif($post->post_author != $user_id && !in_array(get_current_user_id(), $volunteers)): ?>
  <div id='response'></div>
  <div id="ajax-attend-buttons">
    <div id='attend-button' data-status='add' class='button attend-button'>Attend this cleanup</div>
  </div>
  <!-- <form method="post" action="" id='add'>
    <input type="hidden" name="add_remove" value="add" />
    <input type="hidden" name="cleanup_id" value="<?php echo get_the_ID(); ?>" />
    <input type="hidden" name="add-volunteer" value="1" />
  </form> -->


<?php elseif(!empty($volunteers) && in_array($user_id, $volunteers)): ?>
  <div id='response'>
    <p>
      You're registered to attend this cleanup!
      <br>You'll receive a reminder email one week before the event.
    </p>
  </div>
  <div id="ajax-attend-buttons">
    <div id='unattend-button' data-status='remove' class='button attend-button'>Cancel my attendance</div>
  </div>
<?php endif; ?>
