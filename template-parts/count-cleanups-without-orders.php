<?php

$no_order = array();
$hide_menu_message_array = array();

if (is_user_logged_in()) {

  $user_id = get_current_user_id();
  $args = array(
    'post_type' => 'cleanups',
    'author' => $user_id,
  );
  $user_cleanups = get_posts($args);

  foreach($user_cleanups as $cleanup) {
    if (!get_field('orders', $cleanup->ID)) {
      array_push($no_order, true);
    } else {
      array_push($no_order, false);
    }

    if (get_field('hide_order_menu_message', $cleanup->ID)) {
      array_push($hide_menu_message_array, true);
    } else {
      array_push($hide_menu_message_array, false);
    }
  }
}

// if ($no_order == true) {
//   echo 'no order yet boi';
// }
 ?>
