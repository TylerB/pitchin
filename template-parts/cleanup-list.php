<?php
$post_id = get_the_id();
$location = get_field('address');
$volunteers = get_field('volunteers', $post_id, false);
if (!is_array($volunteers)) {
  $volunteers = (array) $volunteers;
}
$user_id = get_current_user_id();

if ($user_id == get_post_field( 'post_author', $post_id )) {
  $author_check = true;
} else {
  $author_check = false;
}
?>

<div class="marker card" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
    <?php echo '<p class="marker-map"><a href="https://www.google.com/maps/place/' . urlencode( $location['address'] ) . '"><img src="http://maps.googleapis.com/maps/api/staticmap?center=' . urlencode( $location['lat'] . ',' . $location['lng'] ). '&zoom=13&size=200x204&maptype=roadmap&sensor=false&markers=color:red%7C' . $location['lat'] . ',' . $location['lng'] . '&key=AIzaSyB7C00SeJl7wEaoMwrehvMVJGfW0YJD440" /></a></p>'; ?>
    <div class="content">
        <div class="title col">
            <a href="<?php the_permalink(); ?>">
                <h2><?php the_title(); ?></h2>
            </a>
            <p class="address"><?php echo $location['address']; ?></p>
            <?php if(get_field('date')): ?>
            <div class="date">
                <?php $start_date = get_field('date'); $start_date = new DateTime($start_date); echo $start_date->format('j M Y'); if (get_field('end_date')) { echo ' - '; $end_date = get_field('end_date'); $end_date = new DateTime($end_date); echo $end_date->format('j M Y'); }; ?>
            </div>
            <?php endif; ?>
        </div>

        <?php $post_group = get_field('group'); ?>
        <?php if(!empty($post_group)): ?>
        <div class="group col">
            <a href="<?php the_permalink($post_group[0]->ID); ?>"><?php echo $post_group[0]->post_title; ?></a>
        </div>
        <?php endif; ?>

        <?php if (!get_field('orders', $post_id) && $author_check): ?>
        <a href="<?php the_permalink(1101); ?>" class='button cleanup-button'>Order Supplies</a>
        <?php else: ?>
        <a href="<?php the_permalink($post_id); ?>" class='button cleanup-button'>View Cleanup</a>
        <?php endif; ?>

        <div class="date-container col">

        </div>
    </div>
    <section
        class="counter <?php if (is_user_logged_in() && in_array($user_id, $volunteers)): echo 'attending-this'; elseif($author_check): echo 'my-cleanup'; endif; ?>">
        <div class="attendee-count">
            <?php
      $number_of_attendees = get_field('number_of_attendees');
      $number_of_volunteers = get_field('volunteers');
      if ($number_of_volunteers) {
        $number_of_volunteers = count($number_of_volunteers);
      } else {
        $number_of_volunteers = 0;
      }
      $number_of_attendees = $number_of_attendees + $number_of_volunteers;
      ?>
            <div class="number"><?php echo $number_of_attendees; ?> <i class="fa fa-users" aria-hidden="true"></i></div>
            <?php if (is_user_logged_in() && in_array($user_id, $volunteers)): ?>
            <div class="subtitle">Volunteers including you</div>
            <?php else: ?>
            <div class="subtitle">Attending <?php if($author_check): echo 'your'; else: echo 'this'; endif;?> cleanup
            </div>
            <?php endif; ?>
        </div>
        <ul class="social-share">
            <li><a target='_blank'
                    href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink($post_id); ?>"><i
                        class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
            <li><a target='_blank' href="https://twitter.com/home?status=<?php the_permalink($post_id); ?>"><i
                        class="fa fa-twitter" aria-hidden="true"></i></a></li>
        </ul>
    </section>
</div>